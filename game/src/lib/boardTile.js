/**
 * Class represents a Tile on the game board
 * @extends Phaser.GameObjects.Sprite
 */
class boardTile extends Phaser.GameObjects.Sprite{
    constructor(config, cords, hex, blockedhex, beehex, emiter){
        super(config.scene, config.x, config.y, hex);
        config.scene.add.existing(this);

        this.blocked = false;
        this.posX = cords.x;
        this.posY = cords.y;

        this.emiter = emiter;
        this.hex = hex;
        this.blockedhex = blockedhex;
        this.beehex = beehex;
        
        this.setInteractive({ useHandCursor: true  } );
        this.on('pointerup', this.clickedTile, this);
    }

    /**
     * Remove tile tint, emit tileBlocked signal
     */
    clickedTile(){
        this.emiter.emit('tileBlocked', this.posX, this.posY);
    }

    /**
     * Get block status
     * @return {boolean} The blocked flag status
     */
    getBlocked(){
        return this.blocked;
    }

    /**
     * Disable tile interaction with the user
     */
    disable() {
        this.disableInteractive();
    }

    /**
     * Enable tile interaction with the user
     */
    enable() {
        this.setInteractive({ useHandCursor: true  } );
    }

    /**
     * Block user interation, replace texture
     */
    block() {
        this.blocked = true;
        this.disableInteractive();
        this.setTexture(this.blockedhex);
    }

    /**
     * Allow user interaction, replace texture
     */
    unblock() {
        this.blocked = false;
        this.setInteractive({ useHandCursor: true  } );
        this.setTexture(this.hex)
    }

    /**
     * Block user interaction, replace texture
     */
    setBee() {
        this.blocked=true;
        this.disableInteractive();
        this.setTexture(this.beehex);
    }

}
