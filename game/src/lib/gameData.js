/**
 * Class keeps important data during the game
 */
class gameData {
    /**
     * Create a empty gameData
     */
    constructor(Size, startBlocked, data, restart, score){
        this.size = {x:Size.x, y:Size.y};
        this.graph = new Graph();
        this.exits = new Array();
        this.history = new playHistory();
        this.userBlocks = 0;
        this.score = {bee: score.bee, block: score.block};


        if(!restart){
            this.bee={x:0,y:0};
            this.init(startBlocked);
        }

        else{
            this.bee=point(data.bee.x,data.bee.y);
            this.initRes(data.blocked);
        }
    }

    /**
     * Set game to the inicial state keeping the initial game state
     * @param {number} blocked number of initial blocked tiles
     */
    initRes(blocked){
        this.setExits();
        for(let i=0; i<this.size.x; i++){
            for(let j=0; j<this.size.y; j++){
                let node = i + (j*this.size.x);
                this.graph.addVertex(node);
            }
        }

        for(let i=0; i<this.size.x; i++){
            for(let j=0; j<this.size.y; j++){
                let connected = this.pointNeighbors({x:i,y:j});
                for(let x=0; x<connected.length; x++){
                    this.graph.addEdge(matToListPoint(connected[x].x,connected[x].y, this.size), matToListPoint(i,j,this.size));
                }
            }
        }
        this.setBlocked(blocked);
        this.history.setStartState(blocked, this.bee);
    }

    /**
     * Set game score bee vs block
     * @param {number} bee Current bee score
     * @param {number} block Current block score
     */
    setScore(bee, block){
        this.score.bee = bee;
        this.score.block = block;
    }

    /**
     * Get current game score
     * @return {{number,number}} Current game score
     */
    getScore(){
        return({bee: this.score.bee, block: this.score.block});
    }

    /**
     * Add one to the current bee score
     */
    upBeeScore(){
        this.score.bee += 1;
    }

    /**
     * Add one to the current block score
     */
    upBlockScore(){
        this.score.block += 1;
    }

    /**
     * Set blocked tiles
     * @param {{number,number}[]} blocked tiles to block
     */
    setBlocked(blocked){
        for(let i=0; i<blocked.length; i++){
            this.graph.remVertex(matToListPoint(blocked[i].x, blocked[i].y, this.size));
        }
    }

    /**
     * Block a number of random tiles
     * @param {number} num number of tiles to block
     * @return {{number,number}[]} The list of blocked tiles
     */
    randBlock(num){
        let i = 0;
        let blocked = [];
        let bpos = matToListPoint(this.bee.x,this.bee.y,this.size);
        while(i<num){
            let pos = Math.floor(Math.random() * (this.size.x*this.size.y));
            if(bpos == pos){
                continue;
            }
            if(this.graph.exists(pos)){
                this.graph.remVertex(pos);
                blocked.push(listToMatPoint(pos,this.size));
                i++;
            }
        }
        return blocked;
    }

    /**
     * Start a new game, generate new start state
     * @param {number} startBlocked Number of initial blocked tiles
     */
    init(startBlocked){
        this.setExits();
        let blocked;
        let validStart = false;
        while(!validStart){
            this.graph.emptyGraph();
            blocked = [];
            // inicial graph vertex/edges
            for(let i=0; i<this.size.x; i++){
                for(let j=0; j<this.size.y; j++){
                    let node = i + (j*this.size.x);
                    this.graph.addVertex(node);
                }
            }

            for(let i=0; i<this.size.x; i++){
                for(let j=0; j<this.size.y; j++){
                    let connected = this.pointNeighbors({x:i,y:j});
                    for(let x=0; x<connected.length; x++){
                        this.graph.addEdge(matToListPoint(connected[x].x,connected[x].y, this.size), matToListPoint(i,j,this.size));
                    }
                }
            }

            this.setBeeinit();
            blocked = this.randBlock(startBlocked);

            // verificar se ha saida
            if(this.canExit()){
                validStart = true;
            }
        }
        this.history.setStartState(blocked, this.bee);
    }

    /**
     * Check if the bee can exit the board
     * @return {boolean} Bool value, if the bee can exit
     */
    canExit(){
        let exList = [];
        for(let i=0; i<this.exits.length; i++){
            let cord = matToListPoint(this.exits[i].x, this.exits[i].y, this.size);
            exList.push(cord);
        }
        let e = this.graph.canReach(matToListPoint(this.bee.x,this.bee.y,this.size), exList);
        return e;
    }

    /**
     * Get the initial game state
     * @return {{number[], {nubmer,number}}} List of blocked tiles and bee position
     */
    getStartState(){
        let blocked = this.history.getStartBloked();
        let bee = this.history.getBeeStart();
        return {blocked: blocked, bee: bee};
    }


    /**
     * At random block a specific number of free tiles
     * @param {number} num number of tiles to block
     * @return {number[]} List of tilex to block
     */
    randBlock(num){
        let i = 0;
        let blocked = [];
        let bpos = matToListPoint(this.bee.x,this.bee.y,this.size);
        while(i<num){
            let pos = Math.floor(Math.random() * (this.size.x*this.size.y));
            if(bpos == pos){
                continue;
            }
            if(this.graph.exists(pos)){
                this.graph.remVertex(pos);
                blocked.push(listToMatPoint(pos,this.size));
                i++;
            }
        }
        return blocked;
    }

    /**
     * Set bee on a random possition around the middle of the board
     */
    setBeeinit(){
        let minX = Math.floor(this.size.x/2) - 2;
        let maxX = Math.floor(this.size.x/2) + 1;
        let minY = Math.floor(this.size.y/2) - 1;
        let maxY = Math.floor(this.size.y/2) + 2;

        this.bee.x = Math.floor(Math.random() * (maxX - minX)) + minX;
        this.bee.y = Math.floor(Math.random() * (maxY - minY)) + minY;
    }

    /**
     * Get current bee possition
     * @return {number,number} Bee coordenates
     */
    getBeePos(){
        return point(this.bee.x, this.bee.y);
    }

    /**
     * Set initial board exits
     */
    setExits(){
        for(let i = 0; i<this.size.x; i++){
            for(let j = 0; j<this.size.y; j++){
                if(j==0 || j==this.size.y-1){
                    this.exits.push({x:i,y:j});
                    continue;
                }
                if((i==0 && j%2==0) || (i==this.size.x-1 && j%2==1)){
                    this.exits.push({x:i,y:j});
                    continue;
                }
            }
        }
    }

    /**
     * Get all the blocked tiles
     * @return {{number,number}{}} List of blocked tiles coordenates
     */
    getBlockedTiles(){
        let ret = [];
        for(let i=0; i<this.size.x;i++){
            for(let j=0; j<this.size.y; j++){
                if(!this.graph.exists(matToListPoint(i,j,this.size))){
                    ret.push(point(i,j));
                }
            }
        }
        return ret;
    }

    /**
     * Block a specific tile
     * @param {number,number} point Tile to block
     */
    block(point){
        this.graph.remVertex(matToListPoint(point.x,point.y,this.size));
        this.history.addBlockPlay({x:point.x,y:point.y});
        this.userBlocks += 1;
    }

    /**
     * Get all tiles next to the one selected
     * @param {number} pos Tile to check
     * @return {numberp[]} List of tiles next to the one requested
     */
    neighbors(pos){
        let ret = this.graph.neighbors(matToListPoint(pos.x,pos.y,this.size));
        return ret;
    }

    /**
     * Get current tiles to where the bee can move
     * @return {number[]} List of tiles to where the bee can move to
     */
    getBeeValidMoves(){
        let ret = this.graph.neighbors(matToListPoint(this.bee.x, this.bee.y, this.size));
        return ret;
    }

    /**
     * Set bee position
     * @param {number.number} pos Position where the bee should be placed
     */
    setBeePos(pos){
        this.bee.x = pos.x;
        this.bee.y = pos.y;
    }

    /**
     * Set bee position and record it on the game history
     * @param {number} pos Next bee position
     */
    setBee(pos){
        this.bee.x = pos.x;
        this.bee.y = pos.y;
        this.history.addBeePlay({ x: pos.x, y: pos.y });
    }


    /**
     * Calculate the best legal bee move from the current position
     * @return {number,number} Best tile for the bee to move to
     */
    calcBeeMove(){
        let exList = [];
        for(let i=0; i<this.exits.length; i++){
            let cord = matToListPoint(this.exits[i].x, this.exits[i].y, this.size);
            exList.push(cord);
        }

        let move = this.graph.pathExitMove(matToListPoint(this.bee.x,this.bee.y,this.size), exList);
        if(move == -1){
            return -1;
        }
        return listToMatPoint(move, this.size);


    }

    /**
     * Check if the bee can legaly make a move
     * @param {number,number} pos Position to check
     * @return {boolean} Boolen to check validity
     */
    validBeeMove(pos){
        let ret = this.graph.neighbors(matToListPoint(this.bee.x, this.bee.y, this.size));
        for(let i=0; i<ret.length; i++){
            let tempP = listToMatPoint(ret[i],this.size);
            if(tempP.x == pos.x && tempP.y == pos.y){
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the bee is currently stuck
     * @return {boolean} The truth of the statement
     */
    beeStuck(){
        let ret = this.graph.neighbors(matToListPoint(this.bee.x, this.bee.y, this.size));
        if(ret.length == 0){
            return true;
        }
    }

    /**
     * Check if the 
     * @param {number} num number of tiles to block
     * @return {{number,number}[]} The list of blocked tiles
     */
    beeEscape(){
        for(let i=0; i<this.exits.length;i++){
            if(this.bee.x==this.exits[i].x && this.bee.y == this.exits[i].y){
                return true;
            }
        }
        return false;
    }

    /**
     * Get the last tile that was blocked
     * @return {number} Last tile that was blocked
     */
    getLastBlockedPos(){
        return this.history.getLastBlocked();
    }

    /**
     * Get last bee position
     * @return {number} Get last tile where the bee was
     */
    getLastBeePos(){
        return this.history.getLastBee();
    }

    /**
     * Undo last block and bee move
     */
    undoLastMove(){
        this.undoBlockMove();
        this.undoBeeMove();
    }

    /**
     * Undo the last block move
     */
    undoBlockMove(){
        let point = this.history.getpopLastBlocked();
        if(point.x == -1){
            return;
        }
        let neig = this.pointNeighbors(point);
        this.graph.addVertex(matToListPoint(point.x,point.y,this.size));
        this.graph.addVertex(matToListPoint(point.x,point.y,this.size));
        for(let i=0; i<neig.length; i++){
            this.graph.addEdge(matToListPoint(point.x,point.y,this.size),matToListPoint(neig[i].x,neig[i].y,this.size));
        }
    }

    /**
     * Undo the last bee move
     */
    undoBeeMove(){
        let point = this.history.getpopLastBee();
        this.bee.x = point.x;
        this.bee.y = point.y;
    }

    /**
     * Get Neighbor tiles to a certain tile
     * @param {number} pos TIle to check
     * @return {number[]} The list of neighbor tiles
     */
    pointNeighbors(pos){
        let ret = new Array();
        if(pos.y % 2 == 0){
            for(let i = pos.x-1; i <= pos.x+1; i++){
                if(i<0 || i>=this.size.x){
                    continue;
                }
                for(let j = pos.y-1; j <= pos.y+1; j++){
                    if(j<0 || j>=this.size.y){
                        continue;
                    }
                    if(i==pos.x && j==pos.y){
                        continue;
                    }
                    if(i==pos.x+1 && (j!=pos.y)){
                        continue;
                    }
                    ret.push({x:i, y:j})
                }
            }
        }
        else{
            for(let i = pos.x-1; i <= pos.x+1; i++){
                if(i<0 || i>=this.size.x){
                    continue;
                }
                for(let j = pos.y-1; j <= pos.y+1; j++){
                    if(j<0 || j>=this.size.y){
                        continue;
                    }
                    if(i==pos.x && j==pos.y){
                        continue;
                    }
                    if(i==pos.x-1 && (j!=pos.y)){
                        continue;
                    }
                    ret.push({x:i, y:j})
                }
            }
        }
        return ret;
    }

}
