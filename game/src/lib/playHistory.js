/** 
 * Class that saves the initial state of a game and
 * his history while it is being played played
 */
class playHistory {
    /**
     * Create playHistory.
     */
    constructor(){
        this.startBlocked = new Array();
        this.beeStart = {x:0,y:0};
        this.blockHist = new Array();
        this.beeHist = new Array();
    }

    /**
     * Set the inicial state of the game
     * @param {[{number,number}]} blocked - The initial blocked tiles
     * @param [{number,number}] bee - The initial bee position
     */
    setStartState(blocked, bee){
        for(let i=0; i<blocked.length; i++){
            this.startBlocked.push(blocked[i]);
        }
        this.beeStart.x = bee.x;
        this.beeStart.y = bee.y;
    }

    /**
     * Get blocked tiles at the start of the game
     * @return {[{number,number}]} Array of blocked tiles
     */
    getStartBloked(){
        return this.startBlocked;
    }

    /**
     * Get inicial bee position
     * @return {{number,number}} The bee position
     */
    getBeeStart(){
        return point(this.beeStart.x,this.beeStart.y);
    }

    /**
     * Add two plays of the game, a tile block and a bee move
     * @param {{number,number}} blockedTile - Tile that was blocked
     * @param {{number,number}} beeMove - Tile to where the bee moved
     */
    addPlay(blockedTile, beeMove){
        this.blockHist.push(point(blockedTile.x,blockedTile.y));
        this.beeHist.push(point(beeMove.x,beeMove.y));
    }

    /**
     * Add block play
     * @param {{number,number}} blockedTile - Tile that was blocked
     */
    addBlockPlay(blockedTile){
        this.blockHist.push(point(blockedTile.x,blockedTile.y));
    }

    /**
     * Add bee position
     * @param {{number,number}} beeMove - The new bee position
     */
    addBeePlay(beeMove){
        this.beeHist.push(point(beeMove.x, beeMove.y));
    }

    getpopLastBee(){
        if(this.beeHist.length==0){
            return this.beeStart;
        }
        let ret =  this.beeHist.pop();
        return ret;
    }

    /**
     * Get the last tile being blocked and remove it from the history
     * @return {{number,number}} The last tile position being blocked
     */
    getpopLastBlocked(){
        if(this.blockHist.length==0){
            return -1;
        }
        return this.blockHist.pop();
    }

    /**
     * Get last bee move in the history
     * @return {{number,number}}
     */
    getLastBee(){
        if(this.beeHist.length <= 1){
            return point(this.beeStart.x,this.beeStart.y);
        }
        return point(this.beeHist[this.beeHist.length-2].x,this.beeHist[this.beeHist.length-2].y);
    }

    /**
     * Get last bee move in the history
     * @return {{number,number}}
     */
    getPrevBee(){
        if(this.beeHist.length==0){
            return point(this.beeStart.x, this.beeStart.y);
        }
        else{
        return point(this.beeHist[this.beeHist.length-1].x,this.beeHist[this.beeHist.length-1].y);
        }
    }

    /**
     * Get last block move in the history
     * @return {{number,number}}
     */
    getLastBlocked(){
        if(this.blockHist.length==0){
            return -1;
        }
        return point(this.blockHist[this.blockHist.length-1].x,this.blockHist[this.blockHist.length-1].y);
    }

}
