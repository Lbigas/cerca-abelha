/**
 * Class represents data about player game achievements
 */
class playerData {
    /**
     * Create inicial playerData
     */
    constructor(){
        this.played = 0;
        this.totalWins = [0,0,0];
        this.totalLoses = [0,0,0];
        this.wins = [0,0,0];
        this.winsFlag = [0,0,0];
        this.loses = [0,0,0];
        this.losesFlag = [0,0,0];
        this.moves = [99,99,99];
    }

    // LocalStorage format:
    // "played": number
    // "wins": ["easy": , "medium": , "hard": ]
    // "loses": ["easy": , "medium": , "hard": ]
    // "moves": ["easy": , "medium": , "hard": ]

    /**
     * Retrieve data saved in the browser's localstorage if it exists
     */
    getLocalData(){
        if(typeof(Storage) === "undefined") {
            return;
        }
        
        let dataAux = localStorage.getItem('TrapTheBeePlayData');
        if(dataAux != null){
            let data = JSON.parse(dataAux);
            this.parseData(data);
        }

    }

    /**
     * Set browser's localstorage accordingly to the current class data
     */
    setLocalData(){
        
        if(typeof(Storage) === "undefined") {
            return;
        }
        
        let storeInfo = {
            'played': this.played, 'moves': this.moves,
            'wins': this.wins, 'loses': this.loses,
            'totalwins': this.totalWins, 'totalloses': this.totalLoses,
            'losesFlag': this.losesFlag, 'winsFlag': this.winsFlag};


        let info = JSON.stringify(storeInfo);

        localStorage.setItem("TrapTheBeePlayData", info);
    }

    /**
     * Parse retreave data from browser's localstorage
     * @param {JSON} data - retrieved data in Json format
     */
    parseData(data) {
        if(Number.isInteger(data['played'])){ // returns false if undefined/null
            this.played = data['played'];
        }
        if(Array.isArray(data['wins'])
           && data['wins'].length == 3
           && this.checkArrayInteger(data['wins'])){
            this.wins = data['wins'].slice();
        }
        
        if(Array.isArray(data['loses'])
           && data['loses'].length == 3
           && this.checkArrayInteger(data['loses'])){

            this.loses = data['loses'].slice();
        }

        if(Array.isArray(data['totalwins'])
           && data['totalwins'].length == 3
           && this.checkArrayInteger(data['totalwins'])){

            this.totalWins = data['totalwins'].slice();
        }
        
        if(Array.isArray(data['totalloses'])
           && data['totalloses'].length == 3
           && this.checkArrayInteger(data['totalloses'])){

            this.totalLoses = data['totalloses'].slice();
        }

        
        if(Array.isArray(data['moves'])
           && data['moves'].length == 3
           && this.checkArrayInteger(data['moves'])){

            this.moves = data['moves'].slice();
        }

        if(Array.isArray(data['winsFlag'])
            && data['winsFlag'].length == 3
            && this.checkArrayInteger(data['winsFlag'])){
                this.winsFlag = data['winsFlag'].slice();
        }


        if(Array.isArray(data['losesFlag'])
            && data['winsFlag'].length == 3
            && this.checkArrayInteger(data['losesFlag'])){
                this.losesFlag = data['losesFlag'].slice();
        }
    }

    /**
     * Check if array only contains integers
     * @param {array} arr - array to check
     * @return {boolean} boolean value of the operation, true -> array only has integers
     */
    checkArrayInteger(arr) {
       
        for (let i = 0; i < arr.length;i++) {
            if(!Number.isInteger(arr[i]) || arr[i] < 0){
                return false;
            }
        }
        return true;
    }
}
