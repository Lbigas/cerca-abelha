/**
 * Create one object with two numbers
 * @param {number} i first number, object named x
 * @param {number} j second number, object named y
 */
function point(i,j){
    return {x:i, y:j}
}

/**
 * Convert a 2 coordenate board position to a absolute position
 * @param {number} i x position
 * @param {number} j y position
 * @param {number,number} Size Board size
 * @return {number} Absolute board position
 */
function matToListPoint(i,j, Size){
    return i + (j*Size.x);
}

/**
 * Convert one absolute board position to a coordenate position
 * @param {number} i Absolute board position
 * @param {number} Size Board size
 * @return {number,number} Coordenate board position
 */
function listToMatPoint(i,Size){
    let y = Math.trunc(i/Size.x);
    let x = i - (y*Size.x);
    return point(x,y);
}

