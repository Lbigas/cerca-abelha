let scenes = [];
scenes.push(preloadScene);
scenes.push(startScene);
scenes.push(mainScene);
scenes.push(gameScene);
scenes.push(achievementTableScene);
scenes.push(rulesScene);
scenes.push(creditsScene);
scenes.push(rankingScene);
scenes.push(loginScene);

var config={
    type: Phaser.CANVAS,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width: 2048,
        height: 1200
    },
    render: {
        pixelArt: false
    },
    backgroundColor: "#f7a41a",
    parent: 'game',
    dom: {
        createContainer: true
    },
    scene: scenes
};


var game = new Phaser.Game(config);
game.scale.fullscreenTarget = document.getElementById(config.parent);

var userData = new playerData();
var infoUser = new loginInfo();
