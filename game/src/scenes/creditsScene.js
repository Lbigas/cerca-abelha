/**
 * Class defined Scene with game credits
 */
class creditsScene extends Phaser.Scene {
    /**
     * Create new empty scene
     */
    constructor() {
        super({
            key: 'creditsScene'
        })

    }

    /**
     * Scene asset preload
     */
    preload() {
    }

    /**
     * Create needed images and text
     */
    create() {

        var gridConfig = {
            'scene': this,
            'cols': 15,
            'rows': 15
        }
        this.aGrid = new AlignGrid(gridConfig);

        this.background = this.add.image(0,0, 'backgroundAchivments');
        this.background.setOrigin(0.5,0.5);
        this.aGrid.placeAtIndex(112,this.background);
        this.background.setScale(1.4);

        this.setFullScreenButton();

        this.title = this.add.image(0, 0, 'cercaAbelha');
        this.title.setScale(0.7);
        this.aGrid.placeAtIndex(37, this.title);


        this.creditosTitle = this.add.image(0, 0, 'creditosTitle');
        this.creditosTitle.setScale(0.2);
        this.creditosTitle.setOrigin(0.48, -0.2);
        this.aGrid.placeAtIndex(26, this.creditosTitle);

        this.ideiaTitle = this.add.text(0, 0, 'Ideia:', { fontFamily: 'myfont2', fontSize: 50, color: '#403217' });
        this.ideiaTitle.setOrigin(0.48);

        this.ideiaText = this.add.text(0, 0, "Ricardo Pinto e Mário Rui Ricardo", { fontFamily: 'myfont2', fontSize: 30, color: '#ffffff' });
        this.ideiaText.setOrigin(0.6, 0.7);

        this.developersTitle = this.add.text(0, 0, "Desenvolvimento:", { fontFamily: 'myfont2', fontSize: 50, color: '#403217' });
        this.developersTitle.setOrigin(0.46);

        this.developersText = this.add.text(0, 0, " Adelino Silva\n José Faria\n Luís Bigas", { fontFamily: 'myfont2', fontSize: 30, color: '#ffffff',align:'left' });
        this.developersText.setOrigin(0.9, 0.1);

        this.aGrid.placeAtIndex(93, this.ideiaTitle);
        this.aGrid.placeAtIndex(111, this.ideiaText);


        this.aGrid.placeAtIndex(124, this.developersTitle);
        this.aGrid.placeAtIndex(140, this.developersText);

        this.direitos = this.add.text(0, 0, "Hypatiamat & Departamento de Informática da Universidade do Minho", { fontFamily: 'myfont2', fontSize:30, color: '#403217', align: 'right' });
        this.direitos.setOrigin(0.5);
        this.aGrid.placeAtIndex(202, this.direitos);

        this.version = this.add.text(0, 0, 'v1.0 Julho 2020', {fontFamily: 'myfont2', fontSize: 23, color: '#403217', align: 'right' });
        this.version.setOrigin(0.5,1.2);
        this.aGrid.placeAtIndex(220, this.version);

        this.helpfullBee = this.add.image(0, 0, "beeRules");
        this.helpfullBee.setScale(1.3);

        this.aGrid.placeAtIndex(178, this.helpfullBee);

        this.back = this.add.sprite(0, 0, "backAchviments");
        this.back.setScale(0.35);
        this.aGrid.placeAtIndex(31, this.back);
        this.back.setOrigin(0.48, 1);


        this.back.setInteractive({ useHandCursor: true });
        this.back.input.hitArea.setTo(-this.back.width/2,-this.back.height/2,
            this.back.width*2, this.back.height*2);

        this.back.on('pointerup', function (pointer) {
                this.scene.transition({
                    target: 'startScene',
                    duration: 1000,
                    moveBelow: true,
                    onUpdate: this.transitionBack
                });
        }, this);

        this.back.on('pointerover', () => {
            this.back.displayHeight += 5;
            this.back.displayWidth += 5;

        });

        this.back.on('pointerout', () => {
            this.back.displayHeight -= 5;
            this.back.displayWidth -= 5;

        });

            
        this.events.on('transitionstart', function (fromScene, duration) {

            this.title.y += game.config.height;
            this.creditosTitle.y += game.config.height;
            this.ideiaTitle.y += game.config.height;
            this.ideiaText.y += game.config.height;
            this.developersTitle.y += game.config.height;
            this.developersText.y += game.config.height;
            this.direitos.y += game.config.height;
            this.helpfullBee.y += game.config.height;
            this.version.y += game.config.height;

            this.tweens.add({
                delay: 1000,
                targets: [this.title, this.creditosTitle, this.ideiaText, this.ideiaTitle, this.developersTitle, this.developersText, this.direitos, this.helpfullBee, this.version],
                durantion: 5000,
                y: '-=' + game.config.height,
                ease: 'Power2'
            });
        }, this);
    }

    /**
     * Exit animation
     * @param {number} progress Animation progress acording to elapsed time
     */
    transitionBack(progress) {
        progress = progress / 9;
        this.title.y = this.title.y + progress * (game.config.width / 4);
        this.creditosTitle.y = this.creditosTitle.y + progress * (game.config.width / 4);
        this.ideiaTitle.y = this.ideiaTitle.y + progress * (game.config.width / 4);
        this.ideiaText.y = this.ideiaText.y + progress * (game.config.width / 4);
        this.developersTitle.y = this.developersTitle.y + progress * (game.config.width / 4);
        this.developersText.y = this.developersText.y + progress * (game.config.width / 4);
        this.direitos.y = this.direitos.y + progress * (game.config.width / 4);
        this.helpfullBee.y = this.helpfullBee.y + progress * (game.config.width / 4);
        this.version.y = this.version.y + progress * (game.config.width / 4);

    }

    /**
     * Button to witch between full screen mode and normal mode
     */
    setFullScreenButton(){
        if(!this.scale.isFullscreen){
            this.fullScreen = this.add.image(0,0, 'fullScreen');
        }
        else{
            this.fullScreen = this.add.image(0, 0, 'noFullScreen');
        }
        this.fullScreen.setOrigin(0.7, 0.4);
        this.fullScreen.setScale(0.5);
        this.aGrid.placeAtIndex(14, this.fullScreen);

        this.fullScreen.setInteractive({ useHandCursor: true });

        this.fullScreen.on('pointerover', () => {
            this.fullScreen.displayHeight += 5;
            this.fullScreen.displayWidth += 5;

        });
        this.fullScreen.on('pointerout', () => {
            this.fullScreen.displayHeight -= 5;
            this.fullScreen.displayWidth -= 5;

        });

        this.fullScreen.on('pointerup', function () {
            if (!this.scale.isFullscreen) {
                this.fullScreen.setTexture('noFullScreen');
                this.scale.startFullscreen();
            }
            else {
                this.scale.stopFullscreen();
                this.fullScreen.setTexture('fullScreen');
            }
        }, this);
    }

}
