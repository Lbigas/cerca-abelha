/**
 * Class of scene to make login to the game
 */
class loginScene extends Phaser.Scene {
    /**
     * Create new empty scene
     */
    constructor() {
        super("loginScene");
    }

    /**
     * Load assets to be ussed
     */
    preload() {}

    /**
     * Create initial assets, and place them
     */
    create() {
        this.input.on('pointerup', () => this.outsideClick());

        var gridConfig = {
            'scene': this,
            'cols': 21,
            'rows': 21
        }

        this.aGrid = new AlignGrid(gridConfig);
        
        this.box = this.add.sprite(0, 0, "endGame");
        this.box.setScale(2);
        this.aGrid.placeAtIndex(199, this.box);

        this.setFullScreenButton();

        let user = `
<input type="text" name="username" style="font-size: 15px;font-family:'myfont';text-align:center;">
`;

        let pass = `
<input type="password" name="passwor" style="font-size: 15px;font-family:'myfont';text-align:center;">
`;

        var x = this.add.dom(0, 0).createFromHTML(user,'game');

        this.aGrid.placeAtIndex(136, x);
        x.setScale(1.5);

        var y = this.add.dom(0, 0).createFromHTML(pass, 'game');
        this.aGrid.placeAtIndex(199, y);
        y.setScale(1.5);

        this.utilizador = this.add.text(0, 0, "Utilizador:", { fontFamily: 'myfont3', fontSize: 30, color: '#403217' })
        this.utilizador.setOrigin(0.5);
        this.aGrid.placeAtIndex(115, this.utilizador);

        this.password = this.add.text(0, 0, "Password:", { fontFamily: 'myfont3', fontSize: 30, color: '#403217' })
        this.password.setOrigin(0.5);
        this.aGrid.placeAtIndex(178, this.password);

        this.logButton = this.add.sprite(0, 0, "greenButton");
        this.logButton.setOrigin(0.5, 0.75);
        this.aGrid.placeAtIndex(283, this.logButton);
        this.logButton.setInteractive({ useHandCursor: true });

        this.login_text = this.add.text(0, 0, "Login!", { fontFamily: 'myfont3', fontSize: 35, color: '#403217' })
        this.login_text.setOrigin(0.5, -0.15);
        this.aGrid.placeAtIndex(262, this.login_text);

        this.logButton.on('pointerup', function () {
            let user = x.getChildByName("username").value
            let password = y.getChildByName("passwor").value
            if (user != '' && password != '') {

                let r = login(user, password,this);
                x.getChildByName("username").value = '';
                y.getChildByName("passwor").value = '';
            }
        }, this);

        this.logButton.on('pointerover', () => {
            this.logButton.displayHeight += 5;
            this.logButton.displayWidth += 5;

        });
        this.logButton.on('pointerout', () => {
            this.logButton.displayHeight -= 5;
            this.logButton.displayWidth -= 5;

        });


        this.person = this.add.image(0, 0, 'person');
        this.person.setScale(0.5);
        this.person.setOrigin(1, 0.5);
        this.aGrid.placeAtIndex(134, this.person);

        this.key = this.add.image(0, 0, 'key');
        this.key.setScale(1);
        this.key.setOrigin(1, 0.5);
        this.aGrid.placeAtIndex(197, this.key);

        this.back_cross = this.add.image(0, 0, 'cross');
        this.back_cross.setScale(1.2);
        this.aGrid.placeAtIndex(97, this.back_cross);
        this.back_cross.setOrigin(0.5, 0.5);
        this.back_cross.setInteractive({ useHandCursor: true });

        this.back_cross.on('pointerover', () => {
            this.back_cross.displayHeight += 5;
            this.back_cross.displayWidth += 5;

        });
        this.back_cross.on('pointerout', () => {
            this.back_cross.displayHeight -= 5;
            this.back_cross.displayWidth -= 5;

        });
        
        this.back_cross.on('pointerup', () => this.exitLoginScene())

        this.loginErrorMsg = this.add.text(0,0,
                                           "Utilizador ou Password Errados",
                                           { fontFamily: 'myfont2',
                                             fontSize: 20,
                                             color: '#ff0000',
                                             align: 'center'});
        
        this.loginErrorMsg.setOrigin(0.5,0);
        this.aGrid.placeAtIndex(220, this.loginErrorMsg);
        this.loginErrorMsg.visible = false;


    }

    /**
     * Button to witch between full screen mode and normal mode
     */
    setFullScreenButton(){
        if(!this.scale.isFullscreen){
            this.fullScreen = this.add.image(0,0, 'fullScreen');
        }
        else{
            this.fullScreen = this.add.image(0, 0, 'noFullScreen');
        }
        this.fullScreen.setOrigin(0.12, 0.3);
        this.fullScreen.setScale(0.5);
        this.aGrid.placeAtIndex(19, this.fullScreen);

        this.fullScreen.setInteractive({ useHandCursor: true });

        this.fullScreen.on('pointerover', () => {
            this.fullScreen.displayHeight += 5;
            this.fullScreen.displayWidth += 5;

        });
        this.fullScreen.on('pointerout', () => {
            this.fullScreen.displayHeight -= 5;
            this.fullScreen.displayWidth -= 5;

        });

        this.fullScreen.on('pointerup', function () {
            if (!this.scale.isFullscreen) {
                this.fullScreen.setTexture('noFullScreen');
                this.scale.startFullscreen();
            }
            else {
                this.scale.stopFullscreen();
                this.fullScreen.setTexture('fullScreen');
            }
        }, this);
    }

    /**
     * Deal with a mouse click outside of the login window
     */
    outsideClick(){
        if(!(this.box.getBounds().contains(this.input.activePointer.x, this.input.activePointer.y))
           && !this.fullScreen.getBounds().contains(this.input.activePointer.x, this.input.activePointer.y)){
            this.exitLoginScene();
        }
    }

    /*
     * Exit this scene
     */
    exitLoginScene(){
        this.scene.stop();
        this.scene.resume('startScene');
    }
}
