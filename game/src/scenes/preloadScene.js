
/** 
 * Class Preloader Scene, to load all assets before game start
 */
class preloadScene extends Phaser.Scene {
    /**
     * Create a new empty scene
     */
    constructor() {
        super("preloadScene");
    }
    
    /**
     * Load game assets
     */
    preload() {

        sessionVerify();

        let x0 = this.game.config.width * 0.1;
        let xSize = this.game.config.width * 0.8;

        let y0 = this.game.config.height * 0.4;
        let ySize = this.game.config.height * 0.2;

        let inyMax = ySize - (ySize - x0 + (xSize*0.01));

        this.progressBarOut = this.add.graphics();
        this.progressBarIn = this.add.graphics();

        let rectout = new Phaser.Geom.Rectangle(x0, y0, xSize, ySize);
        let rectin = new Phaser.Geom.Rectangle(x0 + (xSize*0.01), y0 + (ySize*0.05), xSize - 2*xSize*0.01,  ySize - 2*ySize*0.05);

        this.progressBarOut.fillStyle(0xffffdd, 1);
        this.progressBarOut.fillRectShape(rectout);

        this.progressBarIn.fillStyle(0xfbdc55, 1);
        this.progressBarIn.fillRectShape(rectin);

        this.load.on('progress', (value) =>{
            let rectin = new Phaser.Geom.Rectangle(x0 + (xSize*0.01), y0 + (ySize*0.05), value*(xSize - 2*xSize*0.01),  ySize - 2*ySize*0.05);
            this.progressBarIn.clear();
            this.progressBarIn.fillStyle(0xfbdc55, 1);
            this.progressBarIn.fillRectShape(rectin);
        }
                    )

        
        this.load.path = ('assets/');

        //startScene
        this.load.image('backgroundStart', 'background/backgroundStartScene-01.png');
        this.load.image('helpfullBee', 'bee/helpfullBee.png');
        this.load.image('cercaAbelha', 'text/cercaAbelha.png');
        this.load.image('trofeu', 'buttons/trofeus.png');
        this.load.image('ranking', 'buttons/ranking.png');
        this.load.image('help', 'buttons/help.png');
        this.load.image('info', 'buttons/Info.png');
        this.load.image('play', 'buttons/play.png');
        this.load.image('key', 'buttons/key.png');
        this.load.image('login', 'buttons/login.png');
        this.load.image('greenButton', 'buttons/greenButton.png');
        this.load.image('person','buttons/person.png');
        this.load.image('cross', 'buttons/close.png');
        this.load.image('logout', 'buttons/logout.png');
        this.load.image('noFullScreen', 'buttons/fullScreen1.png');
        this.load.image('fullScreen', 'buttons/fullScreen2.png');

        //mainScene
        this.load.image('backgroundMainScene', 'background/backgroundMainScene.png');
        this.load.image('cercaAbelha2', 'text/cercaAbelha2.png');
        this.load.image("backMainScene", "buttons/backMainScene.png");
        this.load.image("blue", "buttons/1.png");
        
        this.load.image("beeandpot", "bee/beeandpot.png");
        this.load.image("pot1", "bee/pot1.png");
        this.load.image("cap", "bee/cap.png");
        this.load.image("pot2", "bee/pot2.png");

        //gameScene
        this.load.image('backgroundGameScene', 'background/backgroundGameScene-01.png');
        this.load.image('pvpBackGround', 'background/pvpbackground-01.png');
        this.load.image("endGame", "boxes/endGameBackground.png");
        this.load.image("recorde", "extra/recorde1.png");
        this.load.image("TOPRecorde", "extra/recorde2.png");

        // Tiles
        // Empty
        this.load.image("enorm", "tiles/empty/norm.png");
        this.load.image("enorml", "tiles/empty/norml.png");
        this.load.image("eopen1", "tiles/empty/open1.png");
        this.load.image("eopen2", "tiles/empty/open2.png");
        this.load.image("eopen3", "tiles/empty/open3.png");
        this.load.image("eopen4", "tiles/empty/open4.png");

        // blocked tiles
        this.load.image("blnorm", "tiles/blocked/norm.png");
        this.load.image("blnorml", "tiles/blocked/norml.png");
        this.load.image("blopen1", "tiles/blocked/open1.png");
        this.load.image("blopen2", "tiles/blocked/open2.png");
        this.load.image("blopen3", "tiles/blocked/open3.png");
        this.load.image("blopen4", "tiles/blocked/open4.png");

        // bee tiles
        this.load.image("benorm", "tiles/bee/norm.png");
        this.load.image("benorml", "tiles/bee/norml.png");
        this.load.image("beopen1", "tiles/bee/open1.png");
        this.load.image("beopen2", "tiles/bee/open2.png");
        this.load.image("beopen3", "tiles/bee/open3.png");
        this.load.image("beopen4", "tiles/bee/open4.png");

        // Bee normal
        this.load.path = ('assets/');

        // Animacao bee
        this.load.image('animbee1', 'bee/animation/bee1.png');
        this.load.image('animbee2', 'bee/animation/bee2.png');
        this.load.image('animbee3', 'bee/animation/bee3.png');
        this.load.image('animbee4', 'bee/animation/bee4.png');

        // Botao undo
        this.load.image("undo", "buttons/undo.png");

        //Creditos
        this.load.image('creditosTitle', 'text/creditosTitle.png');
     
        //Ranking
        this.load.image('rankBackground', 'background/menuRanking.png');
        this.load.image('TOP', 'text/TOP.png');
        // Achievements
        this.load.image("backgroundAchivments", "background/backgroundAchivments.png");
        this.load.image("backAchviments", "buttons/backAchviments.png");
        this.load.image("trofeusTitle", "text/trofeusTitle.png");
 
        this.load.image("thumb", "extra/slider.png");
        this.load.image("colmeia", "extra/colmeia.png");

        this.load.path = ('assets/achievements/');
        // play
        this.load.image("1", "/play/1.png");
        this.load.image("10", "/play/10.png");
        this.load.image("50", "/play/50.png");
        this.load.image("100", "/play/100.png");

        // lose
        this.load.image("loseE", "/lose/e.png");
        this.load.image("loseM", "/lose/m.png");
        this.load.image("loseH", "/lose/h.png");
        this.load.image("loseA", "/lose/a.png");

        // lose in a row
        this.load.image("loseRE", "/lose/row/e.png");
        this.load.image("loseRM", "/lose/row/m.png");
        this.load.image("loseRH", "/lose/row/h.png");
        this.load.image("loseRA", "/lose/row/a.png");

        //win
        this.load.image("win1E", "/win/1/e.png");
        this.load.image("win1M", "/win/1/m.png");
        this.load.image("win1H", "/win/1/h.png");
        this.load.image("win1A", "/win/1/a.png");

        //win 10
        this.load.image("win10E", "/win/10/e.png");
        this.load.image("win10M", "/win/10/m.png");
        this.load.image("win10H", "/win/10/h.png");
        this.load.image("win10A", "/win/10/a.png");

        //win 25
        this.load.image("win25E", "/win/25/e.png");
        this.load.image("win25M", "/win/25/m.png");
        this.load.image("win25H", "/win/25/h.png");
        this.load.image("win25A", "/win/25/a.png");

        //win in x moves
        this.load.image("winME", "/win/moves/e.png");
        this.load.image("winMM", "/win/moves/m.png");
        this.load.image("winMH", "/win/moves/h.png");

        //win in a row
        this.load.image("winRE", "/win/row/e.png");
        this.load.image("winRM", "/win/row/m.png");
        this.load.image("winRH", "/win/row/h.png");
        this.load.image("winRA", "/win/row/a.png");



        //AJUDA
        this.load.path = ('assets/');
        this.load.image("infoTitle", "text/infoTitle.png");
        this.load.image("coolBee", "bee/happyBee.png");
        this.load.image("beeRules", "bee/beeRules.png");
    }

    /**
     * Start game and stop preloader
     */
    create() {


        //o phaser tem problemas a dar load as fonts, como tal criamos um texto para o far�ar a dar load
        this.add.text(100, 100, "Arial", { font: "1px myfont2", fill: "#FFFFFF" }).visible = false;
        this.add.text(100, 100, "Arial", { font: "1px myfont", fill: "#FFFFFF" }).visible = false;  
        this.add.text(100, 100, "Arial", { font: "1px Sigmar", fill: "#FFFFFF" }).visible = false;
        this.add.text(100, 100, "Arial", { font: "1px myfont3", fill: "#FFFFFF" }).visible = false;

        this.progressBarIn.destroy();
        this.progressBarOut.destroy();
        
        this.scene.transition({
            target: 'startScene',
            duration: 1000,
            onComplete : () => {this.scene.start('startScene'); }
        });

        
    }

    /**
     * Code to execute every frame
     */
    update(){
    }
}
