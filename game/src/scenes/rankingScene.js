/**
 * Class of Scene that shows ranking between players
 */
class rankingScene extends Phaser.Scene {
    /**
     * Create new empty scene
     */
    constructor() {
        super({
            key: 'rankingScene'
        })


    }
    /**
     * Get data passed from calling scene
     * @param {*} data Data 
     */
    init(data) {
        this.array = data;
    }
    /**
     * Preload needed plugin
     */
    preload(){
        this.load.scenePlugin('rexuiplugin', 'src/gridTable.min.js', 'rexUI', 'rexUI');

    }

    /**
     * Create needed images and get ranking values
     */
    create() {
        var gridConfig = {
            'scene': this,
            'cols': 15,
            'rows': 15
        }
        this.aGrid = new AlignGrid(gridConfig);

        var d = new Date();
        var m = d.getMonth();
        var n = d.getFullYear();
        if (m > 7) {
            var x = n;
            var y = n + 1;
        }
        else {
            var x = n - 1;
            var y = n;
        }

        this.di = x + "-09-01";
        this.df = y + "-08-31";
        this.dificulty = 1;
        this.flag = 2;
        //BACKGROUND
        this.background = this.add.image(game.config.width / 2, game.config.height / 2, 'backgroundAchivments');
        this.background.setScale(1.4);

        //TABLEE
        var scrollMode = 0; // 0:vertical, 1:horizontal

        this.table = this.rexUI.add.gridTable({
            x: 1138,
            y: 686,
            width:1575,
            height:706,

            scrollMode: scrollMode,

            background: this.rexUI.add.roundRectangle(0, 0, 20, 10, 10, 0xFFFF00).setAlpha(0.2),

            table: {
                cellWidth: 50,
                cellHeight: 50,
                columns: 6,

                mask: {
                    padding: 2,
                    updateMode: 0,
                },

                reuseCellContainer: true,
            },



            slider: {
                track: this.rexUI.add.roundRectangle(0, 0, 20, 10, 10, 0x260e04),
                thumb: this.add.image(0, 0, "thumb").setScale(0.7),
            },
            space: {
                left: 10,
                right: 26,
                top: 132,
                bottom: 30,

                table: 10,
                header: 10,
                footer: 10,
            },

            createCellContainerCallback: function (cell, cellContainer) {
                let newwith ;

                if (cell.index % 6 == 0) {//index
                    newwith = 10;
                }
                if (cell.index % 6 == 1) {//nome
                    newwith = 10;
                }
                if (cell.index % 6 == 2) {//pontos
                    newwith = 500;
                }
                if (cell.index % 6 == 3) {//Escola
                    newwith = 1305;
                }
                if (cell.index % 6 == 4) {//turm
                    newwith = 2140;
                }
                if (cell.index % 6 == 5) {
                    newwith = 2370;
                }


                var scene = cell.scene,
                    width = newwith,
                    height = cell.height,
                    item = cell.item,
                    index = cell.index,

                        cellContainer = scene.rexUI.add.label({
                            width: width,
                            height: height,

                            orientation: 'top-to-bottom',
                            text: scene.add.text(50, 50, item.name, { fontFamily: "myfont2", fontSize: 21, color: '#000000', align: 'center' }),
                            align: 'center',
                        });

                return cellContainer;
            },
            items: this.CreateItems(600)
        })
            .layout()

        this.aGrid.placeAt(7, 8, this.table);

        this.title = this.add.image(0, 0, 'cercaAbelha');
        this.title.setScale(0.7);
        this.aGrid.placeAtIndex(37, this.title);

        this.topTitle = this.add.image(0, 0, 'TOP');
        this.topTitle.setScale(1.3);
        this.topTitle.setOrigin(0.7, 0);
        this.aGrid.placeAtIndex(26, this.topTitle);


        this.setFullScreenButton();

        this.back = this.add.sprite(0, 0, "backAchviments");
        this.back.setScale(0.35);
        this.aGrid.placeAtIndex(31, this.back);
        this.back.setOrigin(0.44, 1);

        this.back.setInteractive({ useHandCursor: true });
        this.back.input.hitArea.setTo(-this.back.width/2,-this.back.height/2,
            this.back.width*2, this.back.height*2);

        this.back.on('pointerup', function (pointer) {
            this.scene.transition({
                target: 'startScene',
                duration: 1000,
                moveBelow: true,
                onUpdate: this.transitionBack
            });
        }, this);

        this.back.on('pointerover', () => {
            this.back.displayHeight += 5;
            this.back.displayWidth += 5;

        });

        this.back.on('pointerout', () => {
            this.back.displayHeight -= 5;
            this.back.displayWidth -= 5;

        });




        this.container = this.rexUI.add.roundRectangle(0, 0, 200, 700, 0, 0xFFFF00).setAlpha(0.2);
        this.container.setOrigin(0.15, 0.5);
        this.aGrid.placeAtIndex(133, this.container);

        this.lastclick ;

        this.dropdown = this.rexUI.add.gridTable({
            x: 1911,
            y: 490,
            width: 180,
            height: 250,

            scrollMode: scrollMode,

            table: {
                cellWidth: 100,
                cellHeight: 50,
                columns: 1,

                mask: {
                    padding: 2,
                    updateMode: 0,
                },

                reuseCellContainer: true,
            },



            slider: {
                track: this.rexUI.add.roundRectangle(0, 0, 10, 10, 10, 0x260e04),
                thumb: this.rexUI.add.roundRectangle(0, 0, 0, 0, 13, 0x7b5e57),
            },
            space: {
                left: 20,
                right: 0,
                top: 20,
                bottom: 20,

                table: 10,
                header: 10,
                footer: 10,
            },

            createCellContainerCallback: function (cell, cellContainer) {

                var scene = cell.scene,
                    width = cell.width,
                    height = cell.height,
                    item = cell.item,
                    index = cell.index,

                cellContainer = scene.rexUI.add.label({
                    width: width,
                    height: height,

                    orientation: 0,
                    icon: scene.add.circle(0,50,10).setFillStyle('0xffffff'),
                    text: scene.add.text(50, 50, item, { fontFamily: "myfont2", fontSize: 25, color: '#000000', align: 'center' }),
                    align: 'center',
                    space: {
                        icon: 20,
                    }
                });


                var m = d.getMonth();
                var n = d.getFullYear();
                if (m > 7) {
                    var x = n;
                    var y = n + 1;
                }
                else {
                    var x = n - 1;
                    var y = n;
                }

                x = "" + x;
                y = "" + y;

                cellContainer.setInteractive({ useHandCursor: true });
                cellContainer.on('pointerdown', () => {
                    if (scene.lastclick) {
                        scene.lastclick.setFillStyle('0xffffff');
                    }
                    scene.lastclick = cellContainer.getElement('icon').setFillStyle('0x000000');

                    if (cellContainer.getElement('text')._text != "Todos") {
                        scene.di = "20" + cellContainer.getElement('text')._text.split('-')[0] + "-9-1";
                        scene.df = "20"+cellContainer.getElement('text')._text.split('-')[1] + "-8-31";

                    }
                    else {
                        scene.di = "2015-09-01"
                        scene.df = new Date().toISOString().slice(0, 10)
                    }
                    
                    updateTOP(scene.di, scene.df, infoUser.turma, infoUser.escola, scene.flag, scene.dificulty, scene);
                });

                let tmp = x.slice(2, 4) +"-" +y.slice(2,4);
                if (cellContainer.getElement('text')._text == tmp) {
                    scene.lastclick = cellContainer.getElement('icon').setFillStyle('0x000000');
                }

                return cellContainer;


            },
            items: this.selectYear()
        })
            .layout()


        this.ano = this.add.text(0, 0, 'Ano letivo', { fontFamily: 'myfont3', fontSize: 25, color: '#403217' });
        this.ano.setOrigin(0, 0.5);
        this.aGrid.placeAtIndex(73, this.ano);


        this.dificil = this.add.text(0, 0, 'Difícil', { fontFamily: "myfont2",fontSize:25, color: '#000000', align: 'center' });
        this.dificil.setOrigin(-0.47, 0.9);
        this.aGrid.placeAtIndex(133, this.dificil);


        this.hard_icon = this.add.circle(0,0,10).setFillStyle('0xffffff');
        this.hard_icon.setOrigin(0.5, 1.2);
        this.aGrid.placeAtIndex(133, this.hard_icon);
       

        this.normal = this.add.text(0, 0, 'Normal', { fontFamily: "myfont2", fontSize: 25, color: '#000000', align: 'center' });
        this.normal.setOrigin(-0.37, -0.45);
        this.aGrid.placeAtIndex(133, this.normal);


        this.normal_icon = this.add.circle(0,0,10).setFillStyle('0xffffff');
        this.normal_icon.setOrigin(0.5, -1.2);
        this.aGrid.placeAtIndex(133, this.normal_icon);


        this.facil = this.add.text(0, 0, 'Fácil', { fontFamily: "myfont2", fontSize: 25, color: '#000000', align: 'center' });
        this.facil.setOrigin(-0.55, -1.8);
        this.aGrid.placeAtIndex(133, this.facil);


        this.easy_icon = this.add.circle(0,0,10).setFillStyle('0xffffff');
        this.easy_icon.setOrigin(0.5, -3.7);
        this.aGrid.placeAtIndex(133, this.easy_icon);

        this.dificuldade = this.add.text(0, 0, 'Dificuldade', { fontFamily: 'myfont3', fontSize: 25, color: '#403217' });
        this.dificuldade.setOrigin(0, 0);
        this.aGrid.placeAtIndex(118, this.dificuldade);


        this.facil.setInteractive({ useHandCursor: true });
        this.facil.input.hitArea.setTo(-50, -5, this.facil.width + 60, this.facil.height);
        this.facil.on('pointerdown', () => {
            this.hard_icon.setFillStyle('0xffffff');
            this.normal_icon.setFillStyle('0xffffff');
            this.easy_icon.setFillStyle('0x000000');
            this.dificulty = 1;
            updateTOP(this.di, this.df, infoUser.turma, infoUser.escola, this.flag, this.dificulty, this);

         });

        this.normal.setInteractive({ useHandCursor: true });
        this.normal.input.hitArea.setTo(-50, -5, this.normal.width + 60, this.normal.height);

        this.normal.on('pointerdown', () => {
        this.hard_icon.setFillStyle('0xffffff');
        this.normal_icon.setFillStyle('0x000000');
        this.easy_icon.setFillStyle('0xffffff');
        this.dificulty = 2;
        updateTOP(this.di, this.df, infoUser.turma, infoUser.escola, this.flag, this.dificulty, this);
        });

        this.dificil.setInteractive({ useHandCursor: true });
        this.dificil.input.hitArea.setTo(-50, -5, this.dificil.width + 60, this.dificil.height);
        this.dificil.on('pointerdown', () => {
            this.hard_icon.setFillStyle('0x000000');
            this.normal_icon.setFillStyle('0xffffff');
            this.easy_icon.setFillStyle('0xffffff');
            this.dificulty = 3;
            updateTOP(this.di, this.df, infoUser.turma, infoUser.escola, this.flag, this.dificulty, this);
        });

        this.filtro = this.add.text(0, 0, 'Filtro', { fontFamily: 'myfont3', fontSize: 25, color: '#403217' });
        this.filtro.setOrigin(0, 0.5);
        this.aGrid.placeAtIndex(163.3, this.filtro);



        this.turma_filtro = this.add.text(0, 0, 'Turma', { fontFamily: "myfont2", fontSize: 25, color: '#000000', align: 'left' });
        this.turma_filtro.setOrigin(-0.5, -1.3);
        this.aGrid.placeAtIndex(178, this.turma_filtro);
        this.turma_icon = this.add.circle(0,0,10).setFillStyle('0xffffff');

        this.turma_icon.setOrigin(0.5, -2.7);
        this.aGrid.placeAtIndex(178, this.turma_icon);


        this.escola_filtro = this.add.text(0, 0, 'Escola', { fontFamily: "myfont2", fontSize: 25, color: '#000000', align: 'left' });

        this.escola_filtro.setOrigin(-0.45, -0.05);
        this.aGrid.placeAtIndex(178, this.escola_filtro);
        this.escola_icon = this.add.circle(0,0,10).setFillStyle('0xffffff');

        this.escola_icon.setOrigin(0.5, -0.5);
        this.aGrid.placeAtIndex(178, this.escola_icon);

        this.todos = this.add.text(0, 0, 'Todos', { fontFamily: "myfont2", fontSize: 25, color: '#000000', align: 'left' });


        this.todos.setOrigin(-0.5, 1.4);

        this.aGrid.placeAtIndex(178, this.todos);
        this.todos_icon = this.add.circle(0,0,10).setFillStyle('0xffffff');

        this.todos_icon.setOrigin(0.5, 2);
        this.aGrid.placeAtIndex(178, this.todos_icon);

        this.todos.setInteractive({ useHandCursor: true });
        this.todos.input.hitArea.setTo(-50, -5, this.todos.width + 60, this.todos.height);
        this.todos.on('pointerdown', () => {

            this.todos_icon.setFillStyle('0x000000');

            this.escola_icon.setFillStyle('0xffffff');

            this.turma_icon.setFillStyle('0xffffff');

            this.flag = 2;
            updateTOP(this.di, this.df, infoUser.turma, infoUser.escola, this.flag, this.dificulty, this);

        });

        this.escola_filtro.setInteractive({ useHandCursor: true });
        this.escola_filtro.input.hitArea.setTo(-50, -5, this.escola_filtro.width + 60, this.escola_filtro.height);
        this.escola_filtro.on('pointerdown', () => {

            this.todos_icon.setFillStyle('0xffffff');

            this.escola_icon.setFillStyle('0x000000');

            this.turma_icon.setFillStyle('0xffffff');

            this.flag = 1;
            updateTOP(this.di, this.df, infoUser.turma, infoUser.escola, this.flag, this.dificulty, this);
        });
        this.turma_filtro.setInteractive({ useHandCursor: true });
        this.turma_filtro.input.hitArea.setTo(-50, -5, this.turma_filtro.width + 60, this.turma_filtro.height);
        this.turma_filtro.on('pointerdown', () => {

            this.todos_icon.setFillStyle('0xffffff');

            this.escola_icon.setFillStyle('0xffffff');

            this.turma_icon.setFillStyle('0x000000');

            this.flag = 0;

            updateTOP(this.di, this.df, infoUser.turma, infoUser.escola, this.flag, this.dificulty, this);
        });

        this.todos_icon.setFillStyle('0x000000');
        this.easy_icon.setFillStyle('0x000000');
        if (infoUser.user == '') {
            this.filtro.visible = false;
            this.turma_filtro.visible = false;
            this.turma_icon.visible = false;
            this.escola_filtro.visible = false;
            this.escola_icon.visible = false;
            this.todos.visible = false;
            this.todos_icon.visible = false;
        }





        this.events.on('transitionstart', function (fromScene, duration) {
            this.title.y += game.config.height;

            this.table.y += game.config.height;
            this.jogador.y += game.config.height;
            this.pontos.y += game.config.height;
            this.escola.y += game.config.height;
            this.turma.y += game.config.height;
            this.data.y += game.config.height;
            this.dropdown.y += game.config.height;
            this.ano.y += game.config.height;
            this.dificil.y += game.config.height;
            this.hard_icon.y += game.config.height;
            this.normal.y += game.config.height;
            this.normal_icon.y += game.config.height;
            this.facil.y += game.config.height;
            this.easy_icon.y += game.config.height;
            this.dificuldade.y += game.config.height;
            this.filtro.y += game.config.height;
            this.turma_filtro.y += game.config.height;
            this.turma_icon.y += game.config.height;
            this.escola_filtro.y += game.config.height;
            this.escola_icon.y += game.config.height;
            this.todos.y += game.config.height;
            this.todos_icon.y += game.config.height;
            this.container.y += game.config.height;
            this.topTitle.y += game.config.height;

            this.tweens.add({
                delay: 1000,
                targets: [this.table, this.title, this.jogador, this.pontos, this.escola, this.turma, this.data, this.dropdown, this.ano, this.dificil, this.hard_icon, this.normal, this.normal_icon, this.facil, this.easy_icon, this.dificuldade, this.filtro, this.turma_filtro, this.turma_icon, this.escola_filtro, this.escola_icon, this.todos, this.todos_icon, this.container, this.topTitle],
                durantion: 5000,
                y: '-=' + game.config.height,
                ease: 'Power2',
            });

        }, this);


        this.jogador = this.add.text(0, 0, 'Jogador', { fontFamily: 'myfont3', fontSize: 40, color: '#403217' });
        this.jogador.setOrigin(0.4,1);

        this.pontos = this.add.text(0, 0, 'Pontos', { fontFamily: 'myfont3', fontSize: 40, color: '#403217' });
        this.pontos.setOrigin(0.55,1);

        this.escola = this.add.text(0, 0, 'Escola', { fontFamily: 'myfont3', fontSize: 40, color: '#403217' });
        this.escola.setOrigin(0.35,1);

        this.turma = this.add.text(0, 0, 'Turma', { fontFamily: 'myfont3', fontSize: 40, color: '#403217' });
        this.turma.setOrigin(-0.1,1);

        this.data = this.add.text(0, 0, 'Data', { fontFamily: 'myfont3', fontSize: 40, color: '#403217' });
        this.data.setOrigin(0.65,1);



        this.aGrid.placeAtIndex(77, this.jogador);
        this.aGrid.placeAtIndex(79, this.pontos);
        this.aGrid.placeAtIndex(79, this.pontos);
        this.aGrid.placeAtIndex(82, this.escola);
        this.aGrid.placeAtIndex(85, this.turma);
        this.aGrid.placeAtIndex(87, this.data);
    }

    /**
     * Animation to go back to previous scene
     * @param {number} progress Animation progress
     */
    transitionBack(progress) {
        progress = progress / 9;
        this.title.y = this.title.y + progress * (game.config.width / 4);

        this.table.y = this.table.y + progress * (game.config.width / 4);
        this.jogador.y = this.jogador.y + progress * (game.config.width / 4);
        this.pontos.y = this.pontos.y + progress * (game.config.width / 4);
        this.escola.y = this.escola.y + progress * (game.config.width / 4);
        this.turma.y = this.turma.y + progress * (game.config.width / 4);
        this.data.y = this.data.y + progress * (game.config.width / 4);

        this.dropdown.y = this.dropdown.y + progress * (game.config.width / 4);
        this.ano.y = this.ano.y + progress * (game.config.width / 4);
        this.dificil.y = this.dificil.y + progress * (game.config.width / 4);
        this.hard_icon.y = this.hard_icon.y + progress * (game.config.width / 4);
        this.normal.y = this.normal.y + progress * (game.config.width / 4);
        this.normal_icon.y = this.normal_icon.y + progress * (game.config.width / 4);
        this.facil.y = this.facil.y + progress * (game.config.width / 4);
        this.easy_icon.y = this.easy_icon.y + progress * (game.config.width / 4);
        this.dificuldade.y = this.dificuldade.y + progress * (game.config.width / 4);
        this.filtro.y = this.filtro.y + progress * (game.config.width / 4);
        this.turma_filtro.y = this.turma_filtro.y + progress * (game.config.width / 4);
        this.turma_icon.y = this.turma_icon.y + progress * (game.config.width / 4);
        this.escola_filtro.y = this.escola_filtro.y + progress * (game.config.width / 4);
        this.escola_icon.y = this.escola_icon.y + progress * (game.config.width / 4);
        this.todos.y = this.todos.y + progress * (game.config.width / 4);
        this.todos_icon.y = this.todos_icon.y + progress * (game.config.width / 4);
        this.container.y = this.container.y + progress * (game.config.width / 4);
        this.topTitle.y = this.topTitle.y + progress * (game.config.width / 4);
   
    }


    /**
     * Create array from scene data
     * @param {number} count number of items
     */
    CreateItems(count) {
        var data = [];
        for (var i = 0; i < count; i++) {
            if (this.array[i] != "") {
                data.push({
                    name: this.array[i],
                });
            }
        }
        if (this.array.length < 4) {
            return []
        }
        return data;
    }

    /**
     * Select ranking year to check
     * @returns {data} Ranking information
     */
    selectYear() {
        var data = []

        var d = new Date();
        var m = d.getMonth();
        var n = d.getFullYear();
        if (m > 7) {
            var x = n;
            var y = n + 1;
        }
        else {
            var x = n - 1;
            var y = n;
        }
        let di = x + "-09-01";
        let df = y + "-08-31";
        let j = 15;
        for (let i = 2015; i < y; i++) {

            data.push("" + j + "-" + (j + 1));
            j++;
        }
        data.push("Todos");
        data = data.reverse();
        return data;
    }

    /**
     * Button to witch between full screen mode and normal mode
     */
    setFullScreenButton(){
        if(!this.scale.isFullscreen){
            this.fullScreen = this.add.image(0,0, 'fullScreen');
        }
        else{
            this.fullScreen = this.add.image(0, 0, 'noFullScreen');
        }
        this.fullScreen.setOrigin(0.7, 0.4);
        this.fullScreen.setScale(0.5);
        this.aGrid.placeAtIndex(14, this.fullScreen);

        this.fullScreen.setInteractive({ useHandCursor: true });

        this.fullScreen.on('pointerover', () => {
            this.fullScreen.displayHeight += 5;
            this.fullScreen.displayWidth += 5;

        });
        this.fullScreen.on('pointerout', () => {
            this.fullScreen.displayHeight -= 5;
            this.fullScreen.displayWidth -= 5;

        });

        this.fullScreen.on('pointerup', function () {
            if (!this.scale.isFullscreen) {
                this.fullScreen.setTexture('noFullScreen');
                this.scale.startFullscreen();
            }
            else {
                this.scale.stopFullscreen();
                this.fullScreen.setTexture('fullScreen');
            }
        }, this);
    }

}
