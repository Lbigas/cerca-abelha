/**
 * Class of initial game Scene
 */
class startScene extends Phaser.Scene {
    /**
     * Start new scene
     */
    constructor(){
        super("startScene");
    }

    /**
     * Initial scene data
     */
    init(){
        this.bee= [];
        this.stButton;
        this.stButtonXPos;
        this.buttons = new Array(3);
        this.buttonsPos = new Array(this.buttons.length);
    }

    /**
     * Create and place needed assets
     */
    create() {
        var gridConfig = {
            'scene': this,
            'cols': 21,
            'rows': 21
        }
        this.aGrid = new AlignGrid(gridConfig);
        
        this.background= this.add.image(0,0, 'backgroundStart');
        this.aGrid.placeAtIndex(220,this.background);
        this.background.setScale(1.4);

        this.title = this.add.image(0, 0, 'cercaAbelha');
        this.title.setScale(0.7);
        this.title.setOrigin(0.5, 0.58);
        this.aGrid.placeAtIndex(178, this.title);

        this.play = this.add.image(0, 0, 'play');
        this.play.setOrigin(0.3, 0.8);
        this.play.setScale(0.9);
        this.aGrid.placeAtIndex(282, this.play);

        this.setFullScreenButton();

        this.trofeu = this.add.image(0, 0, 'trofeu');
        this.trofeu.setScale(0.45);
        this.trofeu.setOrigin(0.25, 0.3);
        this.aGrid.placeAtIndex(331, this.trofeu);
        
        this.ranking = this.add.image(0, 0, 'ranking');
        this.ranking.setScale( 0.45);
        this.ranking.setOrigin( 0.1 , 0.2);
        this.aGrid.placeAtIndex(353, this.ranking);

        this.login_button = this.add.image(0, 0, 'login');

        this.aGrid.placeAtIndex(38, this.login_button);

        

        
        this.welcometext = this.add.text(0, 0, "Olá " + infoUser.firstName, { fontFamily: 'myfont2', fontSize: 40, color: '#403217' });
        this.welcometext.visible = false;
        this.aGrid.placeAtIndex(24, this.welcometext);
        this.welcometext.setOrigin(0.5,0.6);
       
        this.login_button.setInteractive({useHandCursor: true});

        this.login_button.on('pointerover', () => {
            this.login_button.displayHeight += 5;
            this.login_button.displayWidth += 5;

        });
        this.login_button.on('pointerout', () => {
            this.login_button.displayHeight -= 5;
            this.login_button.displayWidth -= 5;

        });

        this.login_button.on('pointerup', function () {
            this.scene.pause();
            this.scene.launch("loginScene");            
        }, this);


        this.logout = this.add.image(0, 0, 'logout');
        this.logout.setScale(1.5);
        this.aGrid.placeAtIndex(38, this.logout);
        this.logout.visible = false;

        this.logout.setInteractive({ useHandCursor: true });

        this.logout.on('pointerover', () => {
            this.logout.displayHeight += 5;
            this.logout.displayWidth += 5;

        });
        this.logout.on('pointerout', () => {
            this.logout.displayHeight -= 5;
            this.logout.displayWidth -= 5;

        });

        this.logout.on('pointerup', function (pointer) {
                this.logout.visible = !this.logout.visible;
                this.login_button.visible = !this.login_button.visible;
                this.welcometext.visible = false;
                destroySession();

                infoUser.getLocalData();

        }, this);

        if (infoUser.user != '')
        {
            this.login_button.visible = false;;
            let nome = infoUser.firstName.split(" ");
            let nome2 = nome[0] + " " + nome[nome.length - 1];
            this.welcometext.setText("Olá " + nome2);
            this.welcometext.visible = true;   
            this.logout.visible = true;

            
        }
       
               
        this.ranking.on('pointerover', () => {
            this.ranking.displayHeight += 5;
            this.ranking.displayWidth += 5;

        });
        this.ranking.on('pointerout', () => {
            this.ranking.displayHeight -= 5;
            this.ranking.displayWidth -= 5;

        });
        

        this.help = this.add.image(0, 0, 'help');
        this.help.setOrigin(-1.7,0.1);
        this.help.setScale(0.45);
        this.aGrid.placeAtIndex(375, this.help);

        this.info = this.add.image(0, 0, 'info');
        this.info.setScale(0.45);
        this.info.setOrigin(0.45, 0.15);
        this.aGrid.placeAtIndex(351, this.info);

        this.bee = this.add.image(0, 0, 'helpfullBee');
        this.bee.setOrigin(0.55, 0.65);
        this.aGrid.placeAtIndex(342, this.bee );

        this.play.setInteractive({ useHandCursor: true });
        this.play.on('pointerover', () => {
            this.play.displayHeight += 5;
            this.play.displayWidth += 5;

        });
        this.play.on('pointerout', () => {
            this.play.displayHeight -= 5;
            this.play.displayWidth -= 5;

        });

        //TRANSIÇOES
        this.play.on('pointerup', function (pointer) {
                this.tweens.add({
                    targets: this.background,
                    durantion : 1000,
                    alpha: { start: 1, to: 0.45 },
                    ease:'Linear',
                    
                });
                this.tweens.add({
                    targets: [this.info, this.trofeu ,this.ranking ,this.help],
                    durantion : 1000,
                    alpha: { start: 1, to: 0 },
                    ease:'Linear',
                    
                });
                this.scene.transition({
                    target: 'mainScene',
                    duration: 1000,
                    moveBelow: true,
                    onUpdate: this.transitionOut
                });
        }, this);

        this.ranking.setInteractive({ useHandCursor: true });

        this.ranking.on('pointerdown', function (pointer) {
            if (pointer.leftButtonDown()) {
                this.tweens.add({
                    targets: this.background,
                    durantion: 1000,
                    alpha: { start: 1, to: 0.45 },
                    ease: 'Linear',

                });
                this.tweens.add({
                    targets: [this.info, this.trofeu, this.ranking, this.help],
                    durantion: 1000,
                    alpha: { start: 1, to: 0 },
                    ease: 'Linear',

                });



                var d = new Date();
                var m = d.getMonth();
                var n = d.getFullYear();
                if (m > 7) {
                    var x = n;
                    var y = n +1;
                }
                else {
                    var x = n - 1;
                    var y = n;
                }
                let di = x+"-09-01";
                let df = y + "-08-31";
                getTOP(di, df, "", "", 1, this);
            }
                
        }, this);

        this.trofeu.setInteractive({ useHandCursor: true });
        

        this.trofeu.on('pointerover', () => {
            this.trofeu.displayHeight += 5;
            this.trofeu.displayWidth += 5;

        });
        this.trofeu.on('pointerout', () => {
            this.trofeu.displayHeight -= 5;
            this.trofeu.displayWidth -= 5;

        });
        this.trofeu.on('pointerdown', function (pointer) {
            if (pointer.leftButtonDown()) {
                
                this.tweens.add({
                    targets: this.background,
                    durantion : 1000,
                    alpha: { start: 1, to: 0.45 },
                    ease:'Linear',
                    
                });
                this.tweens.add({
                    targets: [this.info, this.trofeu ,this.ranking ,this.help],
                    durantion : 1000,
                    alpha: { start: 1, to: 0 },
                    ease:'Linear',
                    
                });
               this.scene.transition({
                    target: 'achievementTableScene',
                    duration: 1000,
                    moveBelow: true,
                    onUpdate: this.transitionUP
                });
            }
        }, this);


        this.help.setInteractive({ useHandCursor: true });

        
        this.help.input.hitArea.setTo(-2*this.help.width,-this.help.height/2,
                                      this.help.width*5, this.help.height*2);
        

        this.help.on('pointerover', () => {
            this.help.displayHeight += 1;
            this.help.displayWidth += 1;

        });
        this.help.on('pointerout', () => {
            this.help.displayHeight -= 1;
            this.help.displayWidth -= 1;

        });
        this.help.on('pointerdown', function (pointer) {
             if (pointer.leftButtonDown()) {

                this.tweens.add({
                    targets: this.background,
                    durantion : 1000,
                    alpha: { start: 1, to: 0.4 },
                    ease:'Linear',
                    
                });
                this.tweens.add({
                    targets: [this.info, this.trofeu ,this.ranking ,this.help],
                    durantion : 1000,
                    alpha: { start: 1, to: 0 },
                    ease:'Linear',
                    
                });
                this.scene.transition({
                    target: 'rulesScene',
                    duration: 1000,
                    moveBelow: true,
                    onUpdate: this.transitionUP
                });
            }
        }, this);

        this.info.setInteractive({ useHandCursor: true });

        this.info.on('pointerover', () => {
            this.info.displayHeight += 5;
            this.info.displayWidth += 2;

        });
        this.info.on('pointerout', () => {
            this.info.displayHeight -= 5;
            this.info.displayWidth -= 2;

        });
        this.info.on('pointerdown', function (pointer) {
             if (pointer.leftButtonDown()) {
                this.tweens.add({
                    targets: this.background,
                    durantion: 1000,
                    alpha: { start: 1, to: 0.4 },
                    ease: 'Linear',

                });
                this.tweens.add({
                    targets: [this.info, this.trofeu, this.ranking, this.help],
                    durantion: 1000,
                    alpha: { start: 1, to: 0 },
                    ease: 'Linear',

                });
                this.scene.transition({
                    target: 'creditsScene',
                    duration: 1000,
                    moveBelow: true,
                    onUpdate: this.transitionUP
                });
            }
        }, this);


        this.events.on('resume',  ()=> {
            if (infoUser.user != '') {
                console.log("olaa" + this.welcometext);
                this.welcometext.visible = true;
                let nome = infoUser.firstName.split(" ");
                let nome2 = nome[0] + " " + nome[nome.length-1];
                this.welcometext.setText("Olá " + nome2);
                
                this.login_button.visible = false;
                this.logout.visible = true;
            }

            this.checkFullscreen();
        })
       
        this.events.on('transitionstart', function(fromScene, duration){
            if (fromScene === this.scene.get('mainScene') ){
                
                this.tweens.add({
                    delay: 1000,
                    targets: this.background,
                    durantion : 5000,
                    alpha:{ start: 0.45, to: 1 },
                    ease:'Linear'
                });
                this.tweens.add({
                    targets: [this.info, this.trofeu ,this.ranking ,this.help],
                    durantion : 1000,
                    alpha: { start: 0, to: 1 },
                    ease:'Linear',
                    
                });
                
                this.title.x -= game.config.width ;
                this.play.x -= game.config.width ;
                this.bee.x -= game.config.width;
                this.login_button.x -= game.config.width;
                this.logout.x -= game.config.width;
                this.welcometext.x -= game.config.width;
                this.tweens.add({
                    delay: 1000,
                    targets: [this.title, this.play, this.bee, this.login_button,this.logout,this.welcometext] ,
                    x: '+='+game.config.width,
                    durantion : 5000,
                    ease: 'power2',
                    
                });

            }
            if ( fromScene === this.scene.get('PreloadScene')   ) {
                
                this.tweens.add({
                    delay: 100,
                    targets: [this.background, this.trofeu, this.ranking, this.help, this.info, this.play, this.bee, this.login_button, this.logout, this.welcometext, this.fullScreen],
                    durantion : 5000,
                    alpha: { start: 0, to: 1 },
                    ease:'Linear',
                    
                    
                });
                  
                
            }
            if (fromScene === this.scene.get('achievementTableScene') || fromScene === this.scene.get('rulesScene') || fromScene === this.scene.get('rankingScene') || fromScene === this.scene.get('creditsScene') ) {
                this.title.y -= game.config.height ;
                this.play.y -= game.config.height ;
                this.bee.y -= game.config.height ;
                this.login_button.y -= game.config.height;
                this.logout.y -= game.config.height;
                this.welcometext.y -= game.config.height;
               
                this.tweens.add({
                    delay: 1000,
                    targets: this.background,
                    durantion : 3000,
                    alpha: { start: 0.4, to: 1 },
                    ease:'Linear',
                    
                });

                this.tweens.add({
                    delay: 1000,
                    targets: [this.title, this.play, this.bee, this.login_button,this.logout,this.welcometext] ,
                    y: '+=' + game.config.height,
                    durantion : 5000,
                    ease: 'power2'
                    
                });

                this.tweens.add({
                    targets: [this.info, this.trofeu ,this.ranking ,this.help],
                    durantion : 1000,
                    alpha: { start: 0, to: 1 },
                    ease:'Linear',
                    
                });


            }
            
        }, this ) ;
        this.flag = 1;
    }


    /**
     * Check if the current fullscreenbutton is correct
     */
    checkFullscreen(){
        if(!this.scale.isFullscreen){
            this.fullScreen.setTexture('fullScreen');
        }
        else{
            this.fullScreen.setTexture('noFullScreen');
        }
    }


    /**
     * Button to witch between full screen mode and normal mode
     */
    setFullScreenButton(){
        if(!this.scale.isFullscreen){
            this.fullScreen = this.add.image(0,0, 'fullScreen');
        }
        else{
            this.fullScreen = this.add.image(0, 0, 'noFullScreen');
        }
        this.fullScreen.setOrigin(0.12, 0.3);
        this.fullScreen.setScale(0.5);
        this.aGrid.placeAtIndex(19, this.fullScreen);

        this.fullScreen.setInteractive({ useHandCursor: true });

        this.fullScreen.on('pointerover', () => {
            this.fullScreen.displayHeight += 5;
            this.fullScreen.displayWidth += 5;

        });
        this.fullScreen.on('pointerout', () => {
            this.fullScreen.displayHeight -= 5;
            this.fullScreen.displayWidth -= 5;

        });

        this.fullScreen.on('pointerup', function () {
            if (!this.scale.isFullscreen) {
                this.fullScreen.setTexture('noFullScreen');
                this.scale.startFullscreen();
            }
            else {
                this.scale.stopFullscreen();
                this.fullScreen.setTexture('fullScreen');
            }
        }, this);
    }
    /**
     * Animation to send scene to the left
     * @param {number} progress Animation progress 
     */
    transitionOut(progress) {
        progress = progress / 9
        this.title.x = this.title.x - progress * (game.config.width / 4);
        this.play.x = this.play.x - progress * (game.config.width / 4);
        this.bee.x = this.bee.x - progress * (game.config.width / 4);
        this.login_button.x = this.login_button.x - progress * (game.config.width / 4);
        this.logout.x = this.logout.x - progress * (game.config.width / 4);
        this.welcometext.x = this.welcometext.x - progress * (game.config.width / 4);
    }

    /**
     * Animation to send scene to the top
     * @param {number} progress Animation progress 
     */
    transitionUP(progress) {
        progress = progress / 9
        
        this.title.y = this.title.y - progress * (game.config.width / 4);
        this.play.y = this.play.y - progress * (game.config.width / 4);
        this.bee.y = this.bee.y - progress * (game.config.width / 4);
        this.login_button.y = this.login_button.y - progress * (game.config.width / 4);
        this.logout.y = this.logout.y - progress * (game.config.width / 4);
        this.welcometext.y = this.welcometext.y - progress * (game.config.width / 4);
    }
    

    /**
     * Data to update every frame
     */
    update() {
    }
    
}
